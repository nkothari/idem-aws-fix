import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-scaling-policy-" + str(int(time.time())),
    "policy_type": "StepScaling",
    "adjustment_type": "PercentChangeInCapacity",
    "step_adjustments": [
        {
            "MetricIntervalLowerBound": 0,
            "MetricIntervalUpperBound": 15,
            "ScalingAdjustment": 1,
        },
        {
            "MetricIntervalLowerBound": 15,
            "MetricIntervalUpperBound": 25,
            "ScalingAdjustment": 2,
        },
        {"MetricIntervalLowerBound": 25, "ScalingAdjustment": 3},
    ],
    "min_adjustment_magnitude": 1,
    "cooldown": 20,
    "metric_aggregation_type": "Average",
    "min_adjustment_step": 1,
    "enabled": True,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_autoscaling_group, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["auto_scaling_group_name"] = aws_autoscaling_group.get("resource_id")

    ret = await hub.states.aws.autoscaling.scaling_policy.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_scaling_policy(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.autoscaling.scaling_policy.describe(ctx)
    resource_key = f"{PARAMETER.get('auto_scaling_group_name')}/{PARAMETER.get('name')}"
    assert resource_key in describe_ret
    # Verify that describe output format is correct
    assert "aws.autoscaling.scaling_policy.present" in describe_ret[resource_key]
    described_resource = describe_ret[resource_key].get(
        "aws.autoscaling.scaling_policy.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_scaling_policy(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="modify_step_scaling_configuration", depends=["describe"])
async def test_modify_step_scaling_configuration(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["adjustment_type"] = "PercentChangeInCapacity"
    new_parameter["step_adjustments"] = [
        {
            "MetricIntervalLowerBound": 0,
            "MetricIntervalUpperBound": 20,
            "ScalingAdjustment": 1,
        },
        {
            "MetricIntervalLowerBound": 20,
            "MetricIntervalUpperBound": 30,
            "ScalingAdjustment": 2,
        },
        {"MetricIntervalLowerBound": 30, "ScalingAdjustment": 3},
    ]
    new_parameter["min_adjustment_magnitude"] = 2
    new_parameter["cooldown"] = 60
    new_parameter["metric_aggregation_type"] = "Maximum"
    new_parameter["min_adjustment_step"] = 2
    ret = await hub.states.aws.autoscaling.scaling_policy.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_scaling_policy(hub, ctx, ret["old_state"], PARAMETER)
    assert_scaling_policy(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(
    name="test_exec_get_by_autoscaling_group_name",
    depends=["modify_step_scaling_configuration"],
)
async def test_exec_get_by_autoscaling_group_name(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.autoscaling.scaling_policy.get(
        ctx=ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_scaling_policy(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(
    name="test_exec_get_by_policy_name",
    depends=["test_exec_get_by_autoscaling_group_name"],
)
async def test_exec_get_by_autoscaling_policy_name(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.autoscaling.scaling_policy.get(
        ctx=ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_scaling_policy(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(
    name="test_exec_get_by_policy_type", depends=["test_exec_get_by_policy_name"]
)
async def test_exec_get_by_autoscaling_policy_type(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.autoscaling.scaling_policy.get(
        ctx=ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
        policy_type=PARAMETER["policy_type"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_scaling_policy(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(
    name="test_exec_get_invalid_target_group_name",
    depends=["test_exec_get_by_policy_type"],
)
async def test_get_autoscaling_policy_with_invalid_target_group_name(hub, ctx, __test):
    global PARAMETER
    invalid_target_group_name = "invalid-target-group-name"
    ret = await hub.exec.aws.autoscaling.scaling_policy.get(
        ctx=ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=invalid_target_group_name,
        policy_type=PARAMETER["policy_type"],
    )
    # Localstack pro is not giving any error message after failing. only real aws gives this message.
    if not hub.tool.utils.is_running_localstack(ctx):
        assert not ret["result"], ret["comment"]
        assert f"ValidationError" in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(
    name="test_exec_get_invalid_policy_name",
    depends=["test_exec_get_invalid_target_group_name"],
)
async def test_get_autoscaling_policy_with_invalid_policy_name(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.autoscaling.scaling_policy.get(
        ctx=ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
        policy_type=PARAMETER["policy_type"],
        resource_id="invalid-resource-id",
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.get_empty_comment(
            resource_type="aws.autoscaling.scaling_policy", name=PARAMETER["name"]
        )
    ) in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(
    name="test_exec_get_invalid_policy_type",
    depends=["test_exec_get_invalid_policy_name"],
)
async def test_get_autoscaling_policy_with_invalid_policy_type(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.autoscaling.scaling_policy.get(
        ctx=ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
        policy_type="invalid-policy-type",
    )
    # Localstack pro is not giving any error message after failing. only real aws gives this message.
    if not hub.tool.utils.is_running_localstack(ctx):
        assert not ret["result"], ret["comment"]
        assert ("ValidationError") in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["test_exec_get_invalid_policy_type"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.autoscaling.scaling_policy.absent(
        ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_scaling_policy(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.autoscaling.scaling_policy",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.autoscaling.scaling_policy.absent(
        ctx,
        name=PARAMETER["name"],
        auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.autoscaling.scaling_policy",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "auto_scaling_group_name" in PARAMETER:
        ret = await hub.states.aws.autoscaling.scaling_policy.absent(
            ctx,
            name=PARAMETER["name"],
            auto_scaling_group_name=PARAMETER["auto_scaling_group_name"],
        )
        assert ret["result"], ret["comment"]


def assert_scaling_policy(hub, ctx, resource, parameters):
    assert parameters.get("auto_scaling_group_name") == resource.get(
        "auto_scaling_group_name"
    )
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("policy_type") == resource.get("policy_type")
    assert parameters.get("adjustment_type") == resource.get("adjustment_type")
    assert parameters.get("scalable_dimension") == resource.get("scalable_dimension")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert parameters.get("min_adjustment_step") == resource.get(
            "min_adjustment_step"
        )
        assert parameters.get("enabled") == resource.get("enabled")
        assert parameters.get("cooldown") == resource.get("cooldown")
    assert parameters.get("min_adjustment_magnitude") == resource.get(
        "min_adjustment_magnitude"
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        parameters.get("step_adjustments"),
        resource.get("step_adjustments"),
    )
    assert parameters.get("metric_aggregation_type") == resource.get(
        "metric_aggregation_type"
    )
