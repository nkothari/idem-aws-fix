import time
from collections import ChainMap
from typing import List

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-snapshot-" + str(int(time.time())),
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_ec2_volume, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["volume_id"] = aws_ec2_volume["resource_id"]
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    # In real AWS there may be a delay before the volume is ready to snapshot
    if not hub.tool.utils.is_running_localstack(ctx):
        await hub.tool.boto3.client.wait(
            ctx, "ec2", "volume_available", VolumeIds=[PARAMETER["volume_id"]]
        )

    ret = await hub.states.aws.ec2.snapshot.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert f"Would create aws.ec2.snapshot '{PARAMETER['name']}'" in ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert f"Created aws.ec2.snapshot '{PARAMETER['name']}'" in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["volume_id"] == resource.get("volume_id")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.snapshot.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.ec2.snapshot.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.ec2.snapshot.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    assert PARAMETER["volume_id"] == described_resource_map.get("volume_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="get", depends=["describe"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.ec2.snapshot.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource["name"]
    assert PARAMETER["volume_id"] == resource["volume_id"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="list", depends=["get"])
async def test_exec_list(hub, ctx):
    ret = await hub.exec.aws.ec2.snapshot.list(ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) >= 1
    resource = ret["ret"][0]
    assert resource.get("resource_id")
    assert resource.get("name")
    assert resource.get("volume_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["list"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.snapshot.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["volume_id"] == old_resource.get("volume_id")
    if __test:
        assert f"Would delete aws.ec2.snapshot '{PARAMETER['name']}'" in ret["comment"]
    else:
        assert f"Deleted aws.ec2.snapshot '{PARAMETER['name']}'" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.snapshot.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.snapshot '{PARAMETER['name']}' already absent" in ret["comment"]
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_exec_get_invalid_resource_id(hub, ctx):
    snapshot_get_name = "idem-test-exec-get-snapshot-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.snapshot.get(
        ctx, name=snapshot_get_name, resource_id="snap-0d9079577d21e3e32"
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.snapshot '{snapshot_get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
async def test_snapshot_absent_with_none_resource_id(hub, ctx):
    snapshot_temp_name = "idem-test-snapshot-" + str(int(time.time()))
    ret = await hub.states.aws.ec2.snapshot.absent(
        ctx, name=snapshot_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.snapshot '{snapshot_temp_name}' already absent" in ret["comment"]


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.snapshot.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
