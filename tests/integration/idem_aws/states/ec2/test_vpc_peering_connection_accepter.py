import copy
import time
from collections import ChainMap
from typing import Any
from typing import Dict

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

PARAMETER = {
    "name": "idem-test-vpc-peering-connection-accepter-" + str(int(time.time())),
}


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_vpc_peering_connection(
    hub, ctx, aws_ec2_vpc, aws_ec2_vpc_2
) -> Dict[str, Any]:
    """
    Create and cleanup an ec2 vpc peering connection for a module that needs it
    :return: a description of an ec2 vpc peering connection
    """
    global PARAMETER

    vpc_peering_connection_temp_name = "idem-fixture-vpc-peering-connection" + str(
        int(time.time())
    )
    PARAMETER["name"] = vpc_peering_connection_temp_name
    ret = await hub.states.aws.ec2.vpc_peering_connection.present(
        ctx,
        name=vpc_peering_connection_temp_name,
        vpc_id=aws_ec2_vpc_2["VpcId"],
        peer_vpc_id=aws_ec2_vpc["VpcId"],
        peer_owner_id=aws_ec2_vpc["OwnerId"],
        peer_region=ctx["acct"].get("region_name"),
        tags={"idem-test-key-name": vpc_peering_connection_temp_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")

    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "VpcPeeringConnection", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.vpc_peering_connection.absent(
        ctx,
        name=vpc_peering_connection_temp_name,
        resource_id=after["VpcPeeringConnectionId"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_ec2_vpc_peering_connection, __test):
    # Test update with and without a flag
    global PARAMETER
    ctx["test"] = __test

    # Set up initial values
    PARAMETER["connection_status"] = aws_ec2_vpc_peering_connection["Status"]["Code"]
    PARAMETER["resource_id"] = aws_ec2_vpc_peering_connection["VpcPeeringConnectionId"]

    new_parameter = copy.deepcopy(PARAMETER)

    # Update
    new_parameter["connection_status"] = "active"

    ret = await hub.states.aws.ec2.vpc_peering_connection_accepter.present(
        ctx,
        **new_parameter,
    )

    resource = ret["new_state"]
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_status_comment(
                "aws.ec2.vpc_peering_connection_accepter",
                PARAMETER["name"],
                resource.get("resource_id"),
                resource.get("connection_status"),
            )
            + (
                hub.tool.aws.comment_utils.would_update_comment(
                    "aws.ec2.vpc_peering_connection_accepter", PARAMETER["name"]
                )
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_status_comment(
                "aws.ec2.vpc_peering_connection_accepter",
                PARAMETER["name"],
                resource.get("resource_id"),
                resource.get("connection_status"),
            )
            + (
                hub.tool.aws.comment_utils.update_comment(
                    "aws.ec2.vpc_peering_connection_accepter", PARAMETER["name"]
                )
            )
            == ret["comment"]
        )

    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["new_state"]

    assert ret["new_state"]["resource_id"] == ret["old_state"]["resource_id"]
    assert (
        ret["new_state"]["connection_status"] != ret["old_state"]["connection_status"]
    )
    assert ret["new_state"]["connection_status"] == new_parameter["connection_status"]
    assert ret["old_state"]["connection_status"] == PARAMETER["connection_status"]

    PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    # Test describe
    describe_ret = await hub.states.aws.ec2.vpc_peering_connection_accepter.describe(
        ctx
    )
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret

    # Verify that the describe function output format is correct
    assert (
        "aws.ec2.vpc_peering_connection_accepter.present" in describe_ret[resource_id]
    )

    described_resource = describe_ret[resource_id].get(
        "aws.ec2.vpc_peering_connection_accepter.present"
    )

    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["connection_status"] == described_resource_map.get(
        "connection_status"
    )
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    # Test delete with and without a flag. The behavior should be the same either way
    ctx["test"] = __test

    ret = await hub.states.aws.ec2.vpc_peering_connection_accepter.absent(
        ctx, name=PARAMETER["name"]
    )

    assert ret["result"], ret["comment"]
