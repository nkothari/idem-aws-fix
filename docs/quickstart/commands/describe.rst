Describe
========

The *Describe* command will show you how your environment is currently
configured. The output describing your environment can also be used to
configure and update your environment.

Examples
++++++++

VPC
---

List and Describe your VPCs:

.. code-block:: bash

    idem describe aws.ec2.vpc

Output:

.. code-block:: bash

    vpc-07fa0462:
      aws.ec2.vpc.present:
      - name: vpc-07fa0462
      - resource_id: vpc-07fa0462
      - instance_tenancy: default
      - cidr_block_association_set:
        - AssociationId: vpc-cidr-assoc-b2ba2fdb
          CidrBlock: 172.31.0.0/16
          CidrBlockState:
            State: associated
      - enable_dns_hostnames: false
      - enable_dns_support: true

The above example shows info about each VPC. In the next page discussing the
`Idem State </quickstart/commands/state>`_ we'll show how you can use the
output of the `Describe` command to configure the state of existing resources.

Instances
---------

List and Describe your AWS Instances

.. code-block:: bash

    idem describe aws.ec2.instance

Output:

.. code-block:: bash

    i-02e173c663133931c:
      aws.ec2.instance.present:
      - name: i-02e173c663133931c
      - resource_id: i-02e176c664133931c
      - image_id: ami-0a149ac60ba7e9eca
      - instance_type: t2.nano
      - volume_attachments:
          /dev/sda1: vol-050a4a8623f07f89b
      - block_device_mappings:
        - DeviceName: /dev/sda1
          Ebs:
            AttachTime: '2022-09-29 17:22:17+00:00'
            DeleteOnTermination: true
            Status: attached
            VolumeId: vol-051a4b7604f08f89b
      - ebs_optimized: false
      - subnet_id: subnet-caefe28d
      - network_interfaces:
        - AssociatePublicIpAddress: true
          DeleteOnTermination: true
          Description: ''
          DeviceIndex: 0
          Groups:
          - sg-4ce85079
          InterfaceType: interface
          Ipv6Addresses: []
          NetworkCardIndex: 0
          PrivateIpAddresses:
          - Primary: true
            PrivateIpAddress: 172.32.13.39
          SubnetId: subnet-caece28c
      - monitoring_enabled: false
      - root_device_name: /dev/sda1
      - client_token: bc2308c1-dfb9-4067-a49f-fe7ca7c1d0da
      - product_codes: []
      - source_dest_check: true
      - running: true
      - private_ip_address: 172.31.13.39
      - reservation_id: r-062328cd616b961d9
      - owner_id: '713459087031'
      - availability_zone: us-west-2c
      - group_name: ''
      - tenancy: default
      - disable_api_termination: false
      - tags:
          Name: test-idem-cloud-demo
          owner: Tom
      - iam_profile_arn: {}
      - instance_initiated_shutdown_behavior: stop
      - auto_recovery_enabled: true
      - sriov_net_support: simple
      - nitro_enclave_enabled: false
      - license_arns: []
      - hibernation_enabled: false
      - valid_until: ''
      - cpu_credits: standard
      - cpu_core_count: 1
      - cpu_threads_per_core: 1
      - http_tokens: optional
      - http_put_response_hop_limit: 1
      - http_endpoint_enabled: true
      - http_protocol_ipv6_enabled: false
      - metadata_tags_enabled: false
      - hostname_type: ip-name
      - enable_resource_name_dns_a_record: false
      - enable_resource_name_dns_aaaa_record: false
      - capacity_reservation_preference: open
      - bootstrap: []


Users
-----

.. code-block:: console

    idem describe aws.iam.user

Output:

.. code-block:: bash

    iam-user-developer1:
      aws.iam.user.present:
      - name: developer1
      - resource_id: developer1
      - arn: arn:aws:iam::312429933091:user/developer1
      - path: /
      - user_name: developer1
      - tags:
          type: demo-user
